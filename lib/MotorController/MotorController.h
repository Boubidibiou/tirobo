#ifndef __MOTOR_CONTROLLER_H__
#define __MOTOR_CONTROLLER_H__

// Bibliotheque pour le control des moteurs avec une carte ROMEO

class MotorController{

  // Les gains du PID
  float dT = 0.01, Kp = 0.2, Ki = 0.25, Kd = 0.04;

  float minRPM = 0, maxRPM=180; // Vitesse min et max pour protection ces moteurs
  float error = 0, error_prev = 0;
  float RPMset = 0, RPM, OutputRPM = 0, DeltaRPMmax = 20;
  float P, I, D; // Les termes lies au PID
  float sensor_f = 0; // frequence lue par le capteur

  int _DiscSlots = 928; // 928 slot par tour de roue

  unsigned long last_update_time = 0, delta_update = 5; // temps relatif a l'update des moteurs (en millis)
  unsigned long last_interrupt_time;

public:
  byte forward_pin, backward_pin, sensor_pin, pwm_pin;
    // sensor pin public pour initialisation des interrupt

  MotorController(byte _forward_pin, byte _backward_pin, byte _pwm_pin, byte _sensor_pin);
  void InitPin();
  void print();
  float getRPM();
  void SetPIDVal(float _dT, float _Kp, float _Ki, float _Kd);
  void SetRPMBoundaries(float _minRPM, float _maxRPM);
  void SetIntegrationBoundaries(float _Imin, float _Imax);
  float PID();
  void ReadFrequency();
  void Update();
  void Drive(int s);
};

#endif
