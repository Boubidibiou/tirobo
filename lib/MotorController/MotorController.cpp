#include <Arduino.h>
#include "MotorController.h"

// Bibliotheque pour le control des moteurs avec une carte L298N

MotorController::MotorController(byte _forward_pin, byte _backward_pin, byte _pwm_pin, byte _sensor_pin):
forward_pin(_forward_pin),
backward_pin(_backward_pin),
pwm_pin(_pwm_pin),
sensor_pin(_sensor_pin)
{
  // Contructeur de la classe
}

void MotorController::InitPin(){
  // Initialisation des pins
  pinMode(forward_pin, OUTPUT);
  pinMode(backward_pin, OUTPUT);
  pinMode(pwm_pin, OUTPUT);
}

void MotorController::print(){
  // Affichage de toutes les variables
  Serial.print(" error : ");
  Serial.print(error);
  Serial.print(" RPMset : ");
  Serial.print(RPMset);
  Serial.print(" RPM : ");
  Serial.print(RPM);
  Serial.print(" OutputRPM : ");
  Serial.print(OutputRPM);
  Serial.print(" P : ");
  Serial.print(P);
  Serial.print(" I : ");
  Serial.print(I);
  Serial.print(" D : ");
  Serial.println(D);
}

float MotorController::getRPM(){
  // Utile pour debugger et regler les PID
  return RPM;
}

void MotorController::SetPIDVal(float _dT, float _Kp, float _Ki, float _Kd){
  dT = _dT;
  Kp = _Kp;
  Ki = _Ki;
  Kd = _Kd;
}

void MotorController::SetRPMBoundaries(float _minRPM, float _maxRPM){
  minRPM = _minRPM;
  maxRPM = _maxRPM;
}

float MotorController::PID()
{
  // Calcul des valeurs P I D en fonction de l'erreure et des gains
  P = Kp * error; // Composant Proportionnel
  I = Kp * 0.5 * Ki * (error+error_prev) * dT; // Composant integre
  D = Kp * Kd * (error-error_prev) / dT; // Composant derive
  error_prev = error; // enregistrement de l'erreur pour le prochain calcul
  return (P+I+D);
}

void MotorController::ReadFrequency()
{
  // Fonction appele par les interrupt du capteur
  unsigned long c_t = micros(); // on recupere le temps actuel (c_t = current_time)
  unsigned long delta_t = c_t - last_interrupt_time; // on calcul le temps ecoule depuis le dernier interrupt
  last_interrupt_time = c_t; // on enregistre le temps courant pour le prochain interrupt
  sensor_f = 1000000/delta_t; // f in Hz

  // Puis on update les moteurs tous les 'delta_update'
  unsigned long c_t_ms = millis();
  if(c_t_ms - last_update_time > delta_update){
    Update();
    last_update_time = c_t_ms;
  }
}

void MotorController::Update(){
  // Update du moteur : recalcul des RPM, de l'erreur et update de la commande

  RPM = 60.0 * sensor_f / (float)_DiscSlots; // Calcul de la vitesse reelle du moteur
  error = RPMset - RPM; // calcul de l'erreur

  // Calcul de la commande (OutputRPM) grace au PID
  if ( RPMset == 0 ) OutputRPM = 0;
  else OutputRPM = OutputRPM + PID();

  // Protection des moteurs (vitesse max)
  OutputRPM = constrain(OutputRPM, minRPM, maxRPM);

  // Limite de la correction d'erreur : elimine l'accumulation etc.
  OutputRPM = constrain(OutputRPM, 0, RPMset + DeltaRPMmax);

  // ecriture de la commande au moteur
  analogWrite(pwm_pin, (int)(255*OutputRPM/maxRPM));
}

void MotorController::Drive(int s){
  // Fait tourner le moteur dans le sens voulu a la vitesse voulue
  if(s < 0){
    digitalWrite(forward_pin, LOW);
    digitalWrite(backward_pin, HIGH);
  }
  else{
    digitalWrite(forward_pin, HIGH);
    digitalWrite(backward_pin, LOW);
  }
  s = abs(s);
  RPMset = constrain(s, minRPM, maxRPM);
}
