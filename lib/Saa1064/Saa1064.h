#ifndef SAA1064_H
#define SAA1064_H

class Saa1064{
  byte adress = 0x70 >> 1; // Adresse du module saa1064 afficheur 8 segments
  // le code pour afficher le digit sur lecran
  // envoyer digits[3] a l'afficheur lui fera afficher 3
  int digits[16]={63, 6, 91, 79, 102, 109, 125,7, 127, 111, 119, 124, 57, 94, 121, 113};

  public:
  Saa1064();
  void Init();
  void DisplayDigit(int x);
  void ClearDisplay();
};

#endif
