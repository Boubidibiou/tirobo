#include "Arduino.h"
#include "Wire.h"
#include "Saa1064.h"


Saa1064::Saa1064(){
}

void Saa1064::Init(){
 Wire.beginTransmission(adress);
 Wire.write(B00000000); // this is the instruction byte. Zero means the next byte is the control byte
 Wire.write(B01000111); // control byte (dynamic mode on, digits 1+3 on, digits 2+4 on, 12mA segment current
 Wire.endTransmission();
 delay(50);
 ClearDisplay();
}

void Saa1064::DisplayDigit(int x){
  int m, c, d, u; // milliers, centaines, dixaines, unites
  u = x%10;
  d = (x/10)%10;
  c = (x/100)%10;
  m = (x/1000)%10;
  Wire.beginTransmission(adress);
  Wire.write(1); // instruction byte - first digit to control is 1 (right hand side)
  Wire.write(digits[m]); // digit 1
  Wire.write(digits[c]); // digit 2
  Wire.write(digits[d]); // digit 3 // ajouter + 128 pour mettre un point
  Wire.write(digits[u]); // digit 4
  Wire.endTransmission();
}

void Saa1064::ClearDisplay(){
 Wire.beginTransmission(adress);
 Wire.write(1);
 Wire.write(0);
 Wire.write(0);
 Wire.write(0);
 Wire.write(0);
 Wire.endTransmission();
}
