#include <Arduino.h>

#include "Wire.h" // lib pour com i2c

#include "Saa1064.h"
#include "MotorController.h"
#include "AL5D.h"
#include "GY31.h"

#define M1_FORWARD 24
#define M1_BACKWARD 25
#define M1_PWM 6
#define M1_SENSOR_1 18
#define M1_SENSOR_2 19

#define M2_FORWARD 22
#define M2_BACKWARD 23
#define M2_PWM 5
#define M2_SENSOR_1 2
#define M2_SENSOR_2 3

#define SPEED_POT A3

#define NB_SAMPLE 15
#define IR_FRONT A0
#define IR_RIGHT A1
#define IR_LEFT A2

#define LED_FRONT 10
#define LED_RIGHT 9
#define LED_LEFT 11

int led_intensity = 128;
int speed = 0;

// 8 seg display var
Saa1064 saa1064;
int nb_wall = 0;

// Creation des objets moteurs avec les pins associes
MotorController motor_left(M1_FORWARD, M1_BACKWARD, M1_PWM, M1_SENSOR_1);
MotorController motor_right(M2_FORWARD, M2_BACKWARD, M2_PWM, M2_SENSOR_1);

// Arm positions
int zeros[5]       = {0  , 110,   0,   0,   0},
  pose_neutre[5]   = {0  , 110,  80,  95,  88},
  pose_preprise[5] = {0  , 110,  80,   5,  88},
  pose_prise[5]    = {0  , 110, 120,  37,  88},
  joujou_droite[5] = {45 , 110,  50, 150,  10},
  joujou_gauche[5] = {0  , 110,  50, 150, 170},
  rond_droite[5]   = {46 , 110,  80,  95,  88},
  rond_gauche[5]   = {0  , 110,  80,  95,  88},
  rond_haut[5]     = {23 , 110,  60,  95,  88},
  rond_bas[5]      = {23 , 110, 100,  95,  88};

// Creation de l'objet bra robot avec les pins associes
AL5D myArm = AL5D(42, 44, 46, 48, 50 , 52);
int arm_speed = 60;

// Creation de l'objet capteur couleur avec les pins associes
GY31 ColorSens = GY31(45, 47, 51, 53, 49);

/////////////////////////////////////// FONCTIONS ///////////////////////////////////////
float get_distance(int pin){
  // Fonction pour recup la distance des capteurs shark

  int V = 0;
  // lecture de plusieur valeur du capteur
  for (int i=0; i<NB_SAMPLE; i++){
        V += analogRead(pin);
    }
  V /= NB_SAMPLE; // moyenne des valeurs lues

  // Equation calculant la distance avec la tension meusure (issu de la DOC)
  float L = 12.08 * pow(map(V, 0, 1023, 0, 5000)/1000.0, -1.058);
  return L;
}

// C'est moche mais ca marche ; permet de lier les interrupts aux fonctions des moteurs
// impossible de le faire directement car attachInterrupt a besoin de fonction et variables statiques
void m_l_int(){
  // motor_left_interrupt
  motor_left.ReadFrequency();
}

void m_r_int(){
  motor_right.ReadFrequency();
}

/////////////////////////////////////// SETUP ///////////////////////////////////////
void setup() {
  // Optionnal
  Serial.begin(115200);

  // Initialisation communication i2c
  Wire.begin();
  delay(500);
  // Intialisation communication avec afficheur saa1064
  saa1064.Init();

  // Initialisation capteur couleur
  ColorSens.Init();

  // Initialisation bra robot
  myArm.Init();

  // led pin mode
  for(int i = 9; i <= 11; i++){
    pinMode(i, OUTPUT);
  }

  // Motor pin mode
  motor_left.InitPin();
  motor_right.InitPin();

  // Lien des pin et des interrupts
  pinMode(motor_left.sensor_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(motor_left.sensor_pin), m_l_int, CHANGE);
  pinMode(motor_right.sensor_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(motor_right.sensor_pin), m_r_int, CHANGE);
}


/////////////////////////////////////// LOOP ///////////////////////////////////////
void loop(){
  // Recuperation des distances front, left et right
  int d_f = get_distance(IR_FRONT);
  // Serial.print("Distance avant:");
  // Serial.print(analogRead(IR_FRONT));

  int d_r = get_distance(IR_RIGHT);
  // Serial.print(" Distance droite :");
  // Serial.print(analogRead(IR_RIGHT));

  int d_l = get_distance(IR_LEFT);
  // Serial.print(" Distance gauche :");
  // Serial.println(analogRead(IR_LEFT));

  // regalge de la vitesse par rapport au potentiometre
  speed = map(analogRead(SPEED_POT), 0, 1024, 0, 100);

  if(d_f < 15){
    // obstacle devant
    // Serial.println("Obstacle devant");
    nb_wall++; // on compte le nombre de mur
    analogWrite(LED_FRONT, led_intensity); // on allume la led
    motor_left.Drive(-speed); // on recul
    motor_right.Drive(-speed);
    delay(750);
    motor_left.Drive(speed); // on tourne
    motor_right.Drive(-speed);
    delay(500);
  }
  else if(d_l < 15){
    // obstacle a gauche
    // Serial.println("Obstacle a gauche");
    nb_wall++;
    analogWrite(LED_LEFT, led_intensity);
    motor_left.Drive(-speed);
    motor_right.Drive(-speed);
    delay(500);
    motor_left.Drive(speed);
    motor_right.Drive(-speed);
    delay(250);
  }
  else if(d_r < 15){
    //obstacle a droite
    // Serial.println("Obstacle a droite");
    nb_wall++;
    analogWrite(LED_RIGHT, led_intensity);
    motor_left.Drive(-speed);
    motor_right.Drive(-speed);
    delay(500);
    motor_left.Drive(-speed);
    motor_right.Drive(speed);
    delay(250);
  }
  else{
    // Met toutes les leds a zero
    for(int i=9; i<=11; i++){
      analogWrite(i, 0);
    }
    motor_left.Drive(speed);
    motor_right.Drive(speed);
    delay(50);
  }

  saa1064.DisplayDigit(nb_wall);
  // saa1064.DisplayDigit(speed);

  motor_left.Update(); // on appelle l'update des moteurs car, si le robo est a larret,
  motor_right.Update(); // l'uptade nest pas appelle par les interruptions => permet donc de demarrer

  // Pour reglage PID :
  // Serial.print(motor_left.getRPM());
  // Serial.print(" ");
  // Serial.println(motor_right.getRPM());
}
